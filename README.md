# GG Timer

This project is a little timer app to track which person borrowed which peripheral and played for how long.

## What is the GG?
The _GG Gaming & E-Sports Jugendzentrum_ is a youth centre, focused on... well Gaming and eSports. 
If you are between 12 and 27 years old you can go there and play PC, PS4, PS5, Switch, VR and meet with other gamers. For free.

## Free as the GG
If you find this code useful for any reason, take it!
