class TimerController {
    constructor() {
        this.timers = [];
        this.defaultTime = 2700;
        this.timerContainer = document.querySelector('.container')
    }

    addTimer(type, id, time, mouse, keyboard, controller, headset, numberOfControllers, playing) {
        if(!["PC", "VR", "Switch", "PS4", "PS5"].includes(type)) {
            console.error('Wrong Type in Timer Constructor. Should be: ["PC", "VR", "Switch", "PS4", "PS5"]')
            return
        }
        this.timers.push(new Timer(type, id, time, mouse, keyboard, controller, headset, numberOfControllers, playing));
    }

    saveTimers() {
        console.log(this.timers)
        Cookie.set('timers', this.timers);
    }

    loadTimers() {        
        const timers = Cookie.get('timers')
        if(timers.length === 0) {
            console.warn('Could not load timers, building new default ones!');
            this.loadDefaultTimers();
        } else {
            for(let timer of timers) {
                this.addTimer(timer.type, timer.id, timer.time, timer.mouse, timer.keyboard, timer.controller, timer.headset, timer.numberOfControllers, timer.playing)
            }
        }
        console.log(this.timers)
        this.buildHtml()
    }

    refreshTimers() {
        this.counters = {
            "PC": 1,
            "VR": 1,
            "Switch": 1,
            "PS4": 1,
            "PS5": 1,
        }

        this.timers = [];
        this.loadDefaultTimers();
        this.buildHtml();
    }

    loadDefaultTimers() {
        for(let i=1; i<=14; i++) {
            if(i===11){ //VR PC
                continue;
            } 
            this.addTimer('PC', `PCTimer${i.toString().padStart(2, '0')}`, this.defaultTime);
        }
        this.addTimer('Switch', 'SwitchTimer01', this.defaultTime);
        this.addTimer('PS4', 'PS4Timer01', this.defaultTime);
        this.addTimer('PS5', 'PS5Timer01', this.defaultTime);
        this.addTimer('VR', 'VRTimer01', this.defaultTime);
    }

    initEvents() {
        this.timers.forEach(timer => {
            const timerhtml = this.timerContainer.querySelector(`#${timer.id}`);
            const clock = timerhtml.querySelector('.clock');
            const playButton = timerhtml.querySelector('.play-button');
            const resetButton = timerhtml.querySelector('.reset-button');
            const playIcon = timerhtml.querySelector('.fa-play');
            const pauseIcon = timerhtml.querySelector('.fa-pause');
            const plusIcon = timerhtml.querySelector('.fa-plus');
            const minusIcon = timerhtml.querySelector('.fa-minus');
            const controllerCounter = timerhtml.querySelector('.controller_counter');
            const updateTime = () => {
                const minutes = Math.floor(Math.abs(timer.time) / 60).toString().padStart(2, '0');
                const seconds = (Math.abs(timer.time) % 60).toString().padStart(2, '0');
                clock.textContent = `${minutes}:${seconds}`;
            }

            const startTimer = () => {
                playButton.classList.add('playing');
                pauseIcon.style.display = 'block';
                playIcon.style.display = 'none';
                updateTime();
                timer.timerInterval = setInterval(() => {
                    timer.time--;
                    updateTime();
                    if (timer.time <= 300 && timer.time > 0) {
                        timerhtml.classList.add('red');
                    }
                    if (timer.time <= 0) {
                        timerhtml.classList.remove('red');
                        timerhtml.classList.add('overtime');
                    }
                }, 1000);
            }

            timerhtml.querySelectorAll('.periphery .icon').forEach(icon => {
                icon.addEventListener('click', () => {
                    if(icon.classList.contains('fa-plus') || icon.classList.contains('fa-minus')) {
                        return;
                    }
                    icon.classList.toggle('active')
                    if (icon.classList.contains('fa-computer-mouse')){
                        timer.mouse = !timer.mouse;
                    }
                    if (icon.classList.contains('fa-keyboard')){
                        timer.keyboard = !timer.keyboard;
                    }
                    if (icon.classList.contains('fa-gamepad')){
                        timer.controller = !timer.controller;
                    }
                    if (icon.classList.contains('fa-headset')){
                        timer.headset = !timer.headset;
                    }
                })
            });

            playButton.addEventListener('click', () => {
                if (playButton.classList.contains('playing')) {
                    clearInterval(timer.timerInterval);
                    playButton.classList.remove('playing');
                    pauseIcon.style.display = 'none';
                    playIcon.style.display = 'block';
                    timer.playing = false;
                } else {
                    startTimer();
                    timer.playing = true;
                }
            });

            resetButton.addEventListener('click', () => {
                clearInterval(timer.timerInterval);
                timer.time = this.defaultTime;
                updateTime();
                timerhtml.classList.remove('red', 'overtime');
                playButton.classList.remove('playing');
                timer.playing = false;
                pauseIcon.style.display = 'none';
                playIcon.style.display = 'block';
            });

            if(plusIcon !== null) {
                plusIcon.addEventListener('click', () => {
                    timer.numberOfControllers += 1
                    controllerCounter.innerHTML = timer.numberOfControllers.toString().padStart(2, '0');
                })
            }

            if(minusIcon !== null) {
                minusIcon.addEventListener('click', () => {
                    timer.numberOfControllers -= 1
                    timer.numberOfControllers = timer.numberOfControllers >= 0 ? timer.numberOfControllers : 0;
                    controllerCounter.innerHTML = timer.numberOfControllers.toString().padStart(2, '0');
                })
            }

            if(timer.playing) {
                clearInterval(timer.timerInterval);
                startTimer();
            }
        });
    }

    buildHtml(type) {
        let htmlString = ''
        switch(type) {
            case 'used':
                htmlString += '<section class="unused">';
                this.timers.forEach(timer => {
                    if(!timer.playing) {
                        htmlString += timer.buildHtml(timer.type, timer.id);
                    }
                });
                htmlString += '</section>';

                htmlString += '<section class="used">';
                this.timers.forEach(timer => {
                    if(timer.playing) {
                        htmlString += timer.buildHtml(timer.type, timer.id);
                    }
                });
                htmlString += '</section>';
                break;
            case 'room':
                htmlString += '<section class="downstairs">';
                [6,7,4,5,2,3,0,1].forEach(timerNumber => {
                    htmlString += this.timers[timerNumber].buildHtml(this.timers[timerNumber].type, this.timers[timerNumber].id)
                });
                htmlString += '</section>';
                htmlString += '<section class="upstairs">';
                [9,12,10,11].forEach(timerNumber => {
                    htmlString += this.timers[timerNumber].buildHtml(this.timers[timerNumber].type, this.timers[timerNumber].id)
                });
                htmlString += '<div class="dummy-timer"></div>'
                htmlString += this.timers[8].buildHtml(this.timers[8].type, this.timers[8].id)
                htmlString += '</section>';
                htmlString += '<section class="consoles">';
                [14, 15].forEach(timerNumber => {
                    htmlString += this.timers[timerNumber].buildHtml(this.timers[timerNumber].type, this.timers[timerNumber].id)
                });
                htmlString += '<div class="dummy-timer"></div>'
                htmlString += this.timers[13].buildHtml(this.timers[13].type, this.timers[13].id)
                htmlString += '</section>';
                htmlString += '<section class="vr">';
                htmlString += '<div class="dummy-timer"></div>'
                htmlString += this.timers[16].buildHtml(this.timers[16].type, this.timers[16].id)
                htmlString += '</section>';
                break;
            case 'number':
            default:
                htmlString += '<section>';
                this.timers.forEach(timer => {
                    htmlString += timer.buildHtml(timer.type, timer.id);
                });
                htmlString += '</section>';
        }
        this.timerContainer.innerHTML = htmlString;

        console.log(this.timers)
        this.timers.forEach(timer => {
            const timerhtml = this.timerContainer.querySelector(`#${timer.id}`);

            // update Icons
            if (timer.mouse){
                timerhtml.querySelector('.periphery .fa-computer-mouse').classList.add('active')
            }
            if (timer.keyboard){
                timerhtml.querySelector('.periphery .fa-keyboard').classList.add('active')
            }
            if (timer.controller){
                timerhtml.querySelector('.periphery .fa-gamepad').classList.add('active')
            }
            if (timer.headset){
                timerhtml.querySelector('.periphery .fa-headset').classList.add('active')
            }

            if (timer.numberOfControllers) {
                timerhtml.querySelector('.controller_counter').innerHTML = timer.numberOfControllers.toString().padStart(2, '0');
            }

            //update Time
            const minutes = Math.floor(Math.abs(timer.time) / 60).toString().padStart(2, '0');
            const seconds = (Math.abs(timer.time) % 60).toString().padStart(2, '0');
            timerhtml.querySelector('.clock').textContent = `${minutes}:${seconds}`;
        });

        this.initEvents()
    }
}

class Timer {
    constructor(type='PC', id, time, mouse=false, keyboard=false, controller=false, headset=false, numberOfControllers=0, playing=false) {
        this.type = type;
        this.id = id;
        this.time = time;
        this.paused = true;
        this.mouse = mouse;
        this.keyboard = keyboard;
        this.controller = controller;
        this.headset = headset;
        this.numberOfControllers = numberOfControllers;
        this.playing = playing;
        this.timerEvent;

    }

    buildHtml(type, id) {
        let htmlString = ''
        if(type === 'PC'){
            htmlString += `<!-- PC Timer ${id.slice(-2)} --><div class="timer" id="${id}"><div class="half">
                           <div class="title">GG<span>${id.slice(-2)}</span></div><div class="periphery">
                           <i class="icon fa-solid fa-computer-mouse"></i><i class="icon fa-solid fa-keyboard"></i>
                           <i class="icon fa-solid fa-headset"></i><i class="icon fa-solid fa-gamepad"></i></div></div>`
        }  else if (type === 'VR') {
            htmlString += `<!-- VR Timer ${id.slice(-2)} --><div class="timer" id="${id}"><div class="half">
                           <div class="title">VR<span>${id.slice(-2)}</span></div><div class="periphery">
                           <i class="icon fa-solid fa-vr-cardboard"></i></div></div>`;
        } else if (type === 'Switch') {       
             htmlString += `<!-- Switch Timer ${id.slice(-2)} --><div class="timer" id="${id}"><div class="half">
                            <div class="title">SW<span>${id.slice(-2)}</span></div><div class="periphery">
                            <i class="icon fa-solid fa-minus"></i></i><span class="controller_counter">00</span><i class="fa-solid fa-gamepad"></i>
                            <i class="fa-solid icon fa-plus"></i></div></div>`;
        } else if (type === 'PS4') {
            htmlString += `<!--  PS4 Timer ${id.slice(-2)} --><div class="timer" id="${id}"><div class="half">
                           <div class="title">PS</i><span>04</span></div><div class="periphery">
                           <i class="icon fa-solid fa-minus"></i></i><span class="controller_counter">00</span><i class="fa-solid fa-gamepad"></i>
                           <i class="fa-solid icon fa-plus"></i></div></div>`;
        } else if (type === 'PS5') {
            htmlString += `<!-- PS5 Timer ${id.slice(-2)} --><div class="timer" id="${id}"><div class="half">
                           <div class="title">PS</i><span>05</span></div><div class="periphery">
                           <i class="icon fa-solid fa-minus"></i></i><span class="controller_counter">00</span><i class="fa-solid fa-gamepad"></i>
                           <i class="fa-solid icon fa-plus"></i></div></div>`;
        }

        htmlString += `<div class="half"><div class="clock">45:00</div><div class="clock-controls"><div class="play-button">
                   <i class="icon fas fa-play"></i><i class="icon fas fa-pause"></i></div><div class="reset-button">
                   <i class="icon fas fa-rotate-left"></i></div></div></div></div>`;
        
        return htmlString;
    }
}

class Cookie {
    static set(name, timers){
        const timerString = JSON.stringify(timers)
        const currentDate = new Date()
        const experationDate = new Date(currentDate.getTime() + 94608000000); // 3 Jahre
        document.cookie = `${name}=${timerString};${experationDate.toUTCString()};path=/`
    }

    static get(name){
        const cookieName = name + '='
        const decodedCookie = decodeURIComponent(document.cookie);
        const cookies = decodedCookie.split(';')
        for(let cookie of cookies){
            cookie = cookie.trim()
            if(cookie.indexOf(cookieName) === 0){
                return JSON.parse(cookie.substr(cookieName.length))
            }
        }
        return []
    }
}

const refreshTimers = document.querySelector('.refreshTimers');
const sortByNumber = document.querySelector('.sortByNumber');
const sortByRoom = document.querySelector('.sortByRoom');
const sortByUsed = document.querySelector('.sortByUsed');
const timerController = new TimerController();
timerController.loadTimers();

refreshTimers.addEventListener('click', () => {
    timerController.refreshTimers();
})
sortByNumber.addEventListener('click', () => {
    timerController.buildHtml('number');
})
sortByRoom.addEventListener('click', () => {
    timerController.buildHtml('room');
})
sortByUsed.addEventListener('click', () => {
    timerController.buildHtml('used');
})

window.onbeforeunload = () => {
    timerController.saveTimers();
};